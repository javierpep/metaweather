package com.jperezp.metaweather.domain.usecases

import com.jperezp.metaweather.repository.retrofit.ResultHandler.handleApiError
import com.jperezp.metaweather.repository.retrofit.ResultHandler.handleSuccess
import com.jperezp.metaweather.domain.util.NetworkManager.isOnline
import com.jperezp.metaweather.data.entities.Woeid
import com.jperezp.metaweather.data.entities.queryLocation
import android.content.Context
import com.jperezp.metaweather.domain.util.noNetworkConnectivityError
import com.jperezp.metaweather.repository.retrofit.ApiResult
import com.jperezp.metaweather.repository.retrofit.WeatherApi
import com.jperezp.metaweather.repository.retrofit.WeatherRepository

class WeatherRepositoryImpl(private val api: WeatherApi,
                            private val context: Context): WeatherRepository {

    override suspend fun getSearch(queryString: String): ApiResult<List<queryLocation>> {
        if (isOnline(context)) {
            return try {
                val response = api.getSearch(queryString)
                if (response.isSuccessful) {
                    handleSuccess(response)
                } else {
                    handleApiError(response)
                }
            } catch (e: Exception) {
                ApiResult.Error(e)
            }
        } else {
            return context.noNetworkConnectivityError()
        }
    }

    override suspend fun getWoeID(woeId: String): ApiResult<Woeid> {
        if (isOnline(context)) {
            return try {
                val response = api.getWoeID(woeId)
                if (response.isSuccessful) {
                    handleSuccess(response)
                } else {
                    handleApiError(response)
                }
            } catch (e: Exception) {
                ApiResult.Error(e)
            }
        } else {
            return context.noNetworkConnectivityError()
        }
    }
}