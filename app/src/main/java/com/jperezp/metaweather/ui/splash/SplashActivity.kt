package com.jperezp.metaweather.ui.splash

import androidx.appcompat.app.AppCompatActivity
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.widget.Toast
import androidx.lifecycle.Observer
import com.jperezp.metaweather.R
import com.jperezp.metaweather.databinding.SplashActivityBinding
import com.jperezp.metaweather.data.entities.Woeid
import com.jperezp.metaweather.ui.main.MainActivity
import org.koin.android.viewmodel.ext.android.viewModel

class SplashActivity : AppCompatActivity() {

    private lateinit var binding: SplashActivityBinding
    private lateinit var initialSearch: String
    private lateinit var initialWoeID: String
    private val splashModel by viewModel<SplashViewModel>()

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SplashActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initialSearch = getString(R.string.initialCity)
        observeInitialSearch()
        observeError()
    }

    private fun observeInitialSearch(){
        splashModel.getInitialSearch(initialSearch)
        splashModel.initialQueryLocation.observe(this, Observer {
            if (it.isNotEmpty() && it != null && it.size > 0) {
                initialWoeID = it.get(0).getWoeid().toString()
                observeInitialWoeID()
            }
        })
    }

    private fun observeError(){
        splashModel.showError.observe(this, Observer {
            if (it != null) {
                showError(it.toString())
            }
        })
    }

    private fun observeInitialWoeID(){
        splashModel.getInitialWoeID(initialWoeID)
        splashModel.initialWoeID.observe(this, Observer {
            if (it != null) {
                startMainActivity(it)
            }
        })
    }

    private fun showError(error: String){
        Toast.makeText(this, error, Toast.LENGTH_LONG).show()
        finish()
    }

    private fun startMainActivity(value : Woeid){
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra("initialWoeID", value as Parcelable)
        startActivity(intent)
    }

    companion object {
    }
}