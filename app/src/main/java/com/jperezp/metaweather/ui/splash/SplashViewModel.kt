package com.jperezp.metaweather.ui.splash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jperezp.metaweather.repository.retrofit.ApiResult
import com.jperezp.metaweather.repository.retrofit.WeatherRepository
import com.jperezp.metaweather.domain.util.SingleLiveEvent
import com.jperezp.metaweather.data.entities.Woeid
import com.jperezp.metaweather.data.entities.queryLocation
import kotlinx.coroutines.launch

class SplashViewModel(private val repository : WeatherRepository) : ViewModel() {

    val initialQueryLocation = MutableLiveData<List<queryLocation>>()
    val initialWoeID = MutableLiveData<Woeid>()

    val showError = SingleLiveEvent<String>()

    fun getInitialSearch(query : String) {
        viewModelScope.launch {
            val result = repository.getSearch(query)
            when (result) {
                is ApiResult.Success -> {
                    initialQueryLocation.value = result.successData
                }
                is ApiResult.Error -> {
                    showError.value = result.exception.message
                }
            }
        }
    }

    fun getInitialWoeID(woeID : String) {
        viewModelScope.launch {
            val result = repository.getWoeID(woeID)
            when (result) {
                is ApiResult.Success -> {
                    initialWoeID.value = result.successData
                }
                is ApiResult.Error -> {
                    showError.value = result.exception.message
                }
            }
        }
    }
}