package com.jperezp.metaweather.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jperezp.metaweather.repository.retrofit.ApiResult
import com.jperezp.metaweather.repository.retrofit.WeatherRepository
import com.jperezp.metaweather.domain.util.SingleLiveEvent
import com.jperezp.metaweather.data.entities.Woeid
import com.jperezp.metaweather.data.entities.queryLocation
import kotlinx.coroutines.launch

class MainViewModel(private val repository : WeatherRepository) : ViewModel() {

    private val mutableWoeID = MutableLiveData<Woeid>()
    val currentWoeId: LiveData<Woeid> get() = mutableWoeID

    val queryLocations = MutableLiveData<List<queryLocation>>()
    val showError = SingleLiveEvent<String>()


    fun setCurrentWoeID(current : Woeid) {
        mutableWoeID.value = current
    }

    fun getCurrentWoeID(woeID : String) {
        viewModelScope.launch {
            val result = repository.getWoeID(woeID)
            when (result) {
                is ApiResult.Success -> {
                    mutableWoeID.value = result.successData
                }
                is ApiResult.Error -> {
                    showError.value = result.exception.message
                }
            }
        }
    }

    fun getSearchLocation(query : String) {
        viewModelScope.launch {
            val result = repository.getSearch(query)
            when (result) {
                is ApiResult.Success -> {
                    queryLocations.value = result.successData
                }
                is ApiResult.Error -> {
                    showError.value = result.exception.message
                }
            }
        }
    }

}