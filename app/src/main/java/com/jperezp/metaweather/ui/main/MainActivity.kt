package com.jperezp.metaweather.ui.main

import android.app.SearchManager
import android.content.Context
import android.database.MatrixCursor
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.cursoradapter.widget.CursorAdapter
import com.jperezp.metaweather.R
import com.jperezp.metaweather.databinding.MainActivityBinding
import com.jperezp.metaweather.data.entities.Woeid
import org.koin.android.viewmodel.ext.android.viewModel
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.lifecycle.Observer


class MainActivity : AppCompatActivity(), SearchView.OnQueryTextListener, SearchView.OnSuggestionListener {

    private lateinit var binding: MainActivityBinding
    private val mainModel by viewModel<MainViewModel>()

    private lateinit var searchView: SearchView
    private lateinit var searchItem: MenuItem
    private lateinit var cursorAdapter: SimpleCursorAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setInitialWoeid()
        setToolbar()
    }

    private fun setToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun setInitialWoeid(){
        intent.extras!!.getParcelable<Woeid>("initialWoeID")?.let {
            mainModel.setCurrentWoeID(it)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        setSearchView(menu)
        observeSuggestions()
        return true
    }

    private fun setSearchView(menu: Menu?){
        val manager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchItem = menu?.findItem(R.id.search_menu)!!
        searchView = searchItem?.actionView as SearchView
        searchView.setSearchableInfo(manager.getSearchableInfo(componentName))

        val from = arrayOf("_id", "title")
        val to = intArrayOf(R.id.woeid, R.id.city)
            cursorAdapter = SimpleCursorAdapter(this, R.layout.item_city, null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        searchView.suggestionsAdapter = cursorAdapter
        searchView.setOnQueryTextListener(this)
        searchView.setOnSuggestionListener(this)
    }

    private fun observeSuggestions(){
        mainModel.queryLocations.observe(this, Observer {
            if (it != null) {
                var cursor = MatrixCursor(arrayOf("_id", "title"))
                for (citi in it) {
                    cursor.addRow(arrayOf(citi.getWoeid(), citi.getTitle()))
                }
                cursorAdapter.changeCursor(cursor)
            }
        })

    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        clearSearchView()
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        newText?.let { if(it.length % 2 == 0) mainModel.getSearchLocation(it) }
        return false
    }

    override fun onSuggestionSelect(position: Int): Boolean {
        return false
    }

    override fun onSuggestionClick(position: Int): Boolean {
        mainModel.queryLocations.value?.get(position)?.let { mainModel.getCurrentWoeID(it.getWoeid().toString()) }
        clearSearchView()
        return true
    }

    private fun clearSearchView(){
        searchView.clearFocus()
        searchView.setQuery("", false)
        searchItem.collapseActionView()
    }
}