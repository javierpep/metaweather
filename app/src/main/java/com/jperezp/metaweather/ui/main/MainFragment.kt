package com.jperezp.metaweather.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.jperezp.metaweather.R
import com.jperezp.metaweather.databinding.MainFragmentBinding
import com.jperezp.metaweather.data.entities.Woeid
import com.jperezp.metaweather.ui.adapters.DatesAdapter
import org.koin.android.viewmodel.ext.android.sharedViewModel

class MainFragment() : Fragment() {

    private val mainModel by sharedViewModel<MainViewModel>()
    private lateinit var binding: MainFragmentBinding
    private lateinit var datesAdapter: DatesAdapter

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = MainFragmentBinding.bind(view)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setView()
        observeCurrentWoeID()
    }

    private fun setView() {
        datesAdapter  = DatesAdapter(context)
        binding.dateList.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.dateList.adapter = datesAdapter
        binding.dateList.isNestedScrollingEnabled = false
        binding.lifecycleOwner = this
        binding.viewModel = mainModel
    }

    private fun observeCurrentWoeID(){
        mainModel.currentWoeId.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                showWoeID(it)
            }
        })
    }

    private fun showWoeID(currentWoeID : Woeid){
        binding.woeid = currentWoeID
        currentWoeID.consolidatedWeather?.get(0)?.weatherStateAbbr.let {
            Glide.with(context)
                .load(getString(R.string.ICON_URL) + it + ".png")
                .into(binding.imageWeather)
        }
        currentWoeID.consolidatedWeather!!.let { datesAdapter.setDates(it) }
    }

}