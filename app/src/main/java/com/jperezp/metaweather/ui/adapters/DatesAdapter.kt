package com.jperezp.metaweather.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.jperezp.metaweather.R
import com.jperezp.metaweather.databinding.ItemDateBinding
import com.jperezp.metaweather.data.entities.ConsolidatedWeather


class DatesAdapter(val context: Context?) : RecyclerView.Adapter<DatesAdapter.DatesViewHolder>() {

    var datesList: List<ConsolidatedWeather?> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DatesViewHolder {
        val viewBinding: ItemDateBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_date, parent, false
        )
        return DatesViewHolder(viewBinding)
    }


    override fun getItemCount(): Int {
        return datesList.size
    }

    override fun onBindViewHolder(holder: DatesViewHolder, position: Int) {
        holder.onBind(position)
    }

    fun setDates(newDates: List<ConsolidatedWeather?>) {
        this.datesList = newDates
        notifyDataSetChanged()
    }

    fun cleanDates() {
        this.datesList = ArrayList()
        notifyDataSetChanged()
    }

    inner class DatesViewHolder(private val viewBinding: ItemDateBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun onBind(position: Int) {
            val iconUrl = context!!.getString(R.string.ICON_URL)
            val row = datesList[position]
            viewBinding.consolidated = row

            if(row?.weatherStateAbbr != null){
                Glide.with(context)
                    .load(iconUrl + row.weatherStateAbbr + ".png")
                    .into(viewBinding.imageWeather)
            }else{
                viewBinding.imageWeather.setBackgroundResource(R.drawable.ic_baseline_broken_image_24)
            }
        }
    }

}


