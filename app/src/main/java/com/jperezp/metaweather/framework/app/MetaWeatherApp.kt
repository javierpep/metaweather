package com.jperezp.metaweather.framework.app

import android.app.Application
import com.testjperezp.domain.koin.apiModule
import com.testjperezp.domain.koin.networkModule
import com.testjperezp.domain.koin.repositoryModule
import com.testjperezp.domain.koin.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MetaWeatherApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@MetaWeatherApp)
            modules(
                listOf(
                    apiModule,
                    viewModelModule,
                    repositoryModule,
                    networkModule)
            )
        }
    }
}