package com.testjperezp.domain.koin

import com.jperezp.metaweather.ui.main.MainViewModel
import com.jperezp.metaweather.ui.splash.SplashViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel {
        MainViewModel(repository = get())
    }

    viewModel {
        SplashViewModel(repository = get())
    }
}