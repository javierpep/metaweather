package com.testjperezp.domain.koin

import android.content.Context
import com.jperezp.metaweather.repository.retrofit.WeatherApi
import com.jperezp.metaweather.repository.retrofit.WeatherRepository
import com.jperezp.metaweather.domain.usecases.WeatherRepositoryImpl
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val repositoryModule = module {

    fun provideWeatherRepository(api: WeatherApi, context: Context): WeatherRepository {
        return WeatherRepositoryImpl(api, context)
    }
    single { provideWeatherRepository(get(), androidContext()) }
}