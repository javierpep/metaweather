package com.jperezp.metaweather.data.entities

import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
class Source(@SerializedName("title")
             @Expose
             var title: String? = null,
             @SerializedName("slug")
             @Expose
             var slug: String? = null,
             @SerializedName("url")
             @Expose
             var url: String? = null,
             @SerializedName("crawl_rate")
             @Expose
             var crawlRate: Int? = null): Parcelable {
}