package com.jperezp.metaweather.data.entities

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class queryLocation {
    @SerializedName("title")
    @Expose
    private var title: String? = null

    @SerializedName("location_type")
    @Expose
    private var locationType: String? = null

    @SerializedName("woeid")
    @Expose
    private var woeid: Int? = null

    @SerializedName("latt_long")
    @Expose
    private var lattLong: String? = null

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getLocationType(): String? {
        return locationType
    }

    fun setLocationType(locationType: String?) {
        this.locationType = locationType
    }

    fun getWoeid(): Int? {
        return woeid
    }

    fun setWoeid(woeid: Int?) {
        this.woeid = woeid
    }

    fun getLattLong(): String? {
        return lattLong
    }

    fun setLattLong(lattLong: String?) {
        this.lattLong = lattLong
    }
}