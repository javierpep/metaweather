package com.jperezp.metaweather.repository.retrofit

import retrofit2.Response

object ResultHandler {
    fun <T : Any> handleApiError(resp: Response<T>): ApiResult.Error {
        val error = ApiErrorUtils.parseError(resp)
        return ApiResult.Error(Exception(error.message))
    }

    fun <T : Any> handleSuccess(response: Response<T>): ApiResult<T> {
        response.body()?.let {
            return ApiResult.Success(it)
        } ?: return handleApiError(response)
    }
}