package com.jperezp.metaweather.repository.retrofit

data class APIError(val message: String) {
    constructor() : this("")
}