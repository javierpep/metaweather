package com.jperezp.metaweather.repository.retrofit

import com.jperezp.metaweather.data.entities.Woeid
import com.jperezp.metaweather.data.entities.queryLocation
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WeatherApi {

    @GET("location/search/?")
    suspend fun getSearch(@Query("query") queryString : String): Response<List<queryLocation>>

    @GET("location/{woeid}/")
    suspend fun getWoeID(@Path("woeid") woeId: String): Response<Woeid>

}