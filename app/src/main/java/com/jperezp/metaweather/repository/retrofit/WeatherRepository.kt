package com.jperezp.metaweather.repository.retrofit

import com.jperezp.metaweather.data.entities.Woeid
import com.jperezp.metaweather.data.entities.queryLocation

interface WeatherRepository {
    suspend fun getSearch(queryString : String) : ApiResult<List<queryLocation>>

    suspend fun getWoeID(woeId: String) : ApiResult<Woeid>
}