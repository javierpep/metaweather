package com.jperezp.metaweather.ui.splash

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import org.junit.Assert
import org.junit.Before
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.inject

class SplashViewModelTest : KoinTest {

    val model: SplashViewModel by inject()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var initSearch : String

    @Before
    fun setup(){
        initSearch = "Bogotá"
    }

    @Test
    fun `searchInitialCityVerifyResultCount`() {
        val expected = 1
        model.getInitialSearch(initSearch)
        model.initialQueryLocation.observeForever(Observer {
            val result = model.initialQueryLocation.value?.size
            Assert.assertTrue(result == expected)
        })
    }

    @Test
    fun `searchInitialWoeIDVerifyResultCount`() {
        model.getInitialSearch(initSearch)
        model.initialQueryLocation.observeForever(Observer {
            model.getInitialWoeID(model.initialQueryLocation.value?.get(0)?.getWoeid().toString())
            model.initialWoeID.observeForever(Observer {
                val result = model.initialWoeID.value?.title
                Assert.assertTrue(result.equals(initSearch))
            })
        })
    }
}