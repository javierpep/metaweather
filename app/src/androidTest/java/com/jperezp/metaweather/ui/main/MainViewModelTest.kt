package com.jperezp.metaweather.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.jperezp.metaweather.ui.splash.SplashViewModel
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.inject

class MainViewModelTest : KoinTest {

    val model: MainViewModel by inject()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    lateinit var testSearch : String
    lateinit var testSearchForWoeID : String
    lateinit var testCountryForWoeID : String

    @Before
    fun setup(){
        testSearch = "San F"
        testSearchForWoeID = "San Francisco"
    }

    @Test
    fun `searchTestResultVerifyResultCount`() {
        val expected = 4
        model.getSearchLocation(testSearch)
        model.queryLocations.observeForever(Observer {
            val result = model.queryLocations.value?.size
            Assert.assertTrue(result == expected)
        })
    }

    @Test
    fun `searchTestResultVerifyWoeID`() {
        testCountryForWoeID = "California"
        model.getSearchLocation(testSearchForWoeID)
        model.queryLocations.observeForever(Observer {
            model.getCurrentWoeID(model.queryLocations.value?.get(0)?.getWoeid().toString())
            model.currentWoeId.observeForever(Observer {
                Assert.assertTrue(testCountryForWoeID.equals(model.currentWoeId.value?.parent?.title))
            })
        })
    }


}