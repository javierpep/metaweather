**META WEATHER**

**REQUERIMIENTOS TÉCNICOS**

| **CARACTERISTICA** | **VERSION** |
| --- | --- |
| IDE Android Studio | 4.2.1+ |
| SDK Compile version | 30 |
| BuildToolsVersion | 30.0.3 |
| MinSDKVersion | 30 |
| Lenguaje | Kotlin 1.4.32 |
| JDK | 1.8 |

**LIBRERIAS PRINCIPALES**

| **NOMBRE** | **VERSION** |
| --- | --- |
| Koin | 2.0.1 |
| Lifecycle | 2.2.0 |
| Retrofit | 2.7.1 |


**ARQUITECTURA Y PATROM**

Se implementó arquitectura Clean, con distribución a cinco capas y patron MVVM.
